SCH PCB TEMPLATE LIB ALTIUM
===========================

## Parameters for new sch components:

| FieldName                     | Value                                                   |
|------------------------------:|---------------------------------------------------------|
| Group                         | RES / CAP / IC / Mech / etc…                            |
| HelpURL                       | URL on digikey / terra / mouser / chipdip / datasheet   |
| Manufacturer                  | ST / Bourns / Panasonic / etc…                          |
| Package                       | 0805 / SOP8 / TQFP64 / etc…                             |
| PartNumber                    | IRFML8244TRPBF / STM32F030F4P6 / CL21B104KBCNNNC / etc… |
| Value*                        | 10.2k / 4.7uF / 4A / etc…                               |
| Tolerance*                    | ±1% / ±5% / etc…                                        |
| ValueManufacturerPartNumber** | =PartNumber+' '+Manufacturer                            |

  \* - if IC - value is empty
  \*\* - field for autogenerate GOSTBOM





  change today